<?php
/**
 * @file
 * Subthemer Blocks UI Form
 * 
 * OM Subthemer blocks editing
 *
 */

/**
 * OM Blocks Form
 *
 */
function om_blocks_edit(&$form_state) {
  
  $node = node_load(arg(1));
      
  drupal_set_title(t('Blocks for %title', array('%title' => check_plain($node->title))));
  
  $regions = om_regions_get($node->nid);
  
  $regions['disabled'] = 'Disabled';
  
  $form['regions'] = array(
    '#type' => 'hidden',
    '#value' => $regions,
  );
  //dsm($form['regions']['#value']);  
    
  $form['blocks'] = array(
    '#type' => 'fieldset',
    '#title' => t(''),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#tree' => TRUE,
  );

  $result = db_query("SELECT bid, title, content, module, region, weight, nid 
                      FROM {om_subthemer_blocks} 
                      WHERE nid = %d 
                      ORDER BY weight ASC", 
                      $node->nid);
  
  $blocks = array();

  while ($record = db_fetch_object($result)) {
    // if the regions changes in the layout edit all previous regions
    // will default to disabled
    if(!isset($regions[$record->region])) $record->region = 'disabled';
		$blocks[] = _om_blocks_edit($record);
  }
  //dsm($blocks);
  
  // add another blank question fields
  $record = new stdClass();
  $record->bid = 0;
  $record->weight = 0;  
  $record->nid = $node->nid;
  _om_new_rows(&$blocks, 5, $record, 'blocks_edit'); 
     
  $form['blocks'] += $blocks; 
  
  //dsm($form['blocks']);
  $form['submit'] = array( '#type' => 'submit', '#value' => t('Save Blocks'), );

  $form['export'] = array( '#type' => 'submit', '#value' => t('Export'), );

  $form['import'] = array( '#type' => 'submit', '#value' => t('Import'), );
    
  return $form;
}

/**
 * blocks Form
 *
 */
function _om_blocks_edit($record) {
 
  $form['bid'] = array(
    '#type' => 'hidden',
    '#value' => $record->bid,
    '#attributes' => array('class' => 'om-bid'),    
  );  
  $form['nid'] = array(
    '#type' => 'textfield',
    '#value' => $record->nid,
  );  
  $form['region'] = array(
    '#type' => 'textfield',
    '#default_value' => ($record->region) ? $record->region: 'disabled',
    '#attributes' => array('class' => 'om-region'),
    '#size' => 15,
  );
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => $record->title,
    '#size' => 15,
  );
  $form['content'] = array(
    '#type' => 'textarea',
    '#title' => t('Content'),
    '#default_value' => $record->content,
    '#cols' => 30,
  );
  $form['weight'] = array(
    '#type' => 'weight',
    '#title' => t('Weight'),
    '#default_value' => $record->weight,
    '#delta' => 50,
    '#attributes' => array('class' => 'om-weight'),
  );
  $form['module'] = array(
    '#type' => 'textfield',
    '#title' => t('Module'),
    '#default_value' => $record->module,
    '#size' => 15,
  );  
  $form['delete'] = array(
    '#type' => ($form['bid']['#value']) ? 'checkbox' : 'hidden',
    '#title' => t('Delete'),
    '#default_value' => 0,
    '#description' => t(''),
  );    
  return $form;
}

/**
 * Implementation of theme_hook()
 * - View in table format
 *
 */
function theme_om_blocks_edit($form) {
  drupal_add_css(drupal_get_path('module', 'om_subthemer') . '/css/om_subthemer.css');

  $output = '';
  //dsm($form['blocks']);
  $rows = array();
  $regions = $form['regions']['#value'];    
  
  //dsm($regions);
  foreach ($regions as $region_key => $region_name) {

    $region = array(
      '#type' => 'hidden',
      '#value' => $region_key,
      '#attributes' => array('class' => 'om-section'),
    );
    $rows[] = array(
      'data' => array('data' => $regions[$region_key], NULL, NULL, drupal_render($region), NULL,),
      'id' => 'om-row-'. $region_key,
      'class' => 'om-row-region',
    ); 

    foreach (element_children($form['blocks']) as $key) {

      if ($form['blocks'][$key]['region']['#value'] == $region_key) {
        
        $row = array();
        // Strips labels
        $form['blocks'][$key]['title']['#title'] = '';
        $form['blocks'][$key]['content']['#title'] = '';
        $form['blocks'][$key]['module']['#title'] = '';
        $form['blocks'][$key]['region']['#title'] = '';        
        $form['blocks'][$key]['weight']['#title'] = '';      
        $form['blocks'][$key]['delete']['#title'] = '';    

        // 0 as parent has no indent
        ($form['blocks'][$key]['region']['#value'] != NULL) ? $indent = '<div class="indentation">&nbsp;</div>': $indent = '';
  
        $row[] = array('class' => 'col-title', 'data' => $indent . drupal_render($form['blocks'][$key]['title']));
        $row[] = array('class' => 'col-content', 'data' => drupal_render($form['blocks'][$key]['content']));
        $row[] = array('class' => 'col-module', 'data' => drupal_render($form['blocks'][$key]['module']));
        $row[] = array('class' => 'col-hidden', 'data' => drupal_render($form['blocks'][$key]['region']) . drupal_render($form['blocks'][$key]['bid']) . drupal_render($form['blocks'][$key]['nid']) . drupal_render($form['blocks'][$key]['weight']));
        $row[] = array('class' => 'col-delete', 'data' => drupal_render($form['blocks'][$key]['delete']));    

        $rows[] = array(
          'data' => $row,
          'id' => 'om-row-'. $key,
          'class' => 'draggable row-'. $key . ' ' . 'tabledrag-leaf',
        ); 
      }
    }
  }
  //dsm($rows);
  $header = array('Regions & Block Titles', 'Content', 'Module', 'Region', 'Delete'); //, 'Update'

  // Header
  $form['blocks']['#children'] = theme('table', $header, $rows, array('id' => 'om-subthemer-blocks'));

  //drupal_add_tabledrag($table_id, $action, $relationship, $group, $subgroup = NULL, $source = NULL, $hidden = TRUE, $limit = 0);
  drupal_add_tabledrag('om-subthemer-blocks', 'match', 'parent', 'om-region', 'om-region', 'om-section', TRUE, -1);
  drupal_add_tabledrag('om-subthemer-blocks', 'order', 'sibling', 'om-weight');
  
  $output .= drupal_render($form);
  
  return $output;
}

/**
 * blocks Submit
 * table: bid, title, content, module, region, weight, nid
 */
function om_blocks_edit_submit($form, $form_state) {

  if($form_state['values']['op'] == t('Save Blocks')) { 
    
    $form_values = $form_state['values'];

    foreach ($form_values['blocks'] as $block) {

      if ($block['delete']) {
        // Delete an existing layout
        db_query("DELETE FROM {om_subthemer_blocks} WHERE bid = %d", $block['bid']);
      
        drupal_set_message(t('The block (' . $block['title'] . ') has been deleted.'));      
      }
      elseif ($block['bid']) { 
      
        db_query("UPDATE {om_subthemer_blocks} SET title = '%s', content = '%s', module = '%s', region = '%s', weight = %d WHERE bid = %d", 
        $block['title'], $block['content'], $block['module'], $block['region'], $block['weight'], $block['bid']);
      
        drupal_set_message(t('The block (' . $block['title'] . ') has been updated.'));
      }
      elseif (!$block['bid'] && !empty($block['title'])) {
      
        db_query("INSERT INTO {om_subthemer_blocks} (title, content, module, region, weight, nid) VALUES ('%s', '%s', '%s', '%s', %d, %d)", 
        $block['title'], $block['content'], $block['module'], $block['region'], $block['weight'], $block['nid']);
      
        drupal_set_message(t('The block (' . $block['title'] . ') has been added.'));
      }
      else {
        // If no condition is set, nothing should happen
      }   
    }
    // files need to be written again
  
    // need this to generate the layout
    //include_once drupal_get_path('module', 'om_subthemer') . '/inc/om_subthemer.write.inc';    
    
    // need to update the files
    //$node = node_load(arg(1));

    // set user folder 
    //om_subthemer_user_folder(&$node);

    // 1) get layout for variables
    //om_process_layout(&$node);
    
    // 2) write om subtheme
    //om_file_write($node); 
  }
  elseif($form_state['values']['op'] == t('Export')) {
    drupal_goto('node/' . arg(1) . '/om-blocks/export');
  }
  elseif($form_state['values']['op'] == t('Import')) {
    drupal_goto('node/' . arg(1) . '/om-blocks/import');
  }          
}






