<?php
/**
 * @file
 * OM Subthemer Layout Engine
 *
 * @author: Daniel Honrade http://drupal.org/user/351112
 * 
 * This is a recursive function which automatically creates:
 *   - Wrapper div - if other elements are nested under it
 *   - Region div  - if no other elements are nested under it
 *   - Variable    - if the tag name is a reserved variable name,
 *                   the list of reserved variable names is in /modules/system/page.tpl.php,
 *                   You can also register other reserve variable name via base theme variable
 * 
 * index.html for previewing and page.tpl.php for the subtheming are generated
 * using this same function
 *
 */


/**
 * Layout Generator
 *
 * @param $divs 
 *   - array elements in the layout, same as $om_layout_content
 *     but starts with array(0 => ...
 * @param $output_type 
 *   - html/om/php
 *
 */ 
function om_subthemer_layout($divs = array(), $output_type = NULL) {
  global $om_layout_content; // layout array
  global $om_regions;        // regions
  global $om_region_classes; // additional classes for regions
  global $om_count;          // count number of arrays
  
	static $output   = '';     // init output
  //static $om_region_classes = NULL; // additional classes for regions	
	$out_content     = '';     // recursive content for final output
  
  static $count    = 0;      // count recursion, if equals number of arrays it will start the output

	$div_count       = 0;      // init every level count of div
	$div_count_total = 0;      // init every level total of div

	static $level    = 0;      // count level for indents on divs on html file
	$level++;                  // start counting
	$tab = '';                 // init tab for indents on divs on html file
	
	// for source formating
	// calculates number of tabs
	for($i = 1; $i < $level; $i++) {
		$tab .= "\t";
	}
	
	// initialize elements object to populate divs with classes, 
	// container tags, and contents
	$elements = new stdClass;
		
	//read array
  foreach ($divs as $cid => $prop) {
  
    // count only columns and rows
  	if ($prop['type'] != 'variable') $div_count++;
  	$div_count_total = count($divs); //counts inner divs
  	
  	// addtional classess
	  $elements->classes = om_layout_classes($div_count_total, $div_count, $prop);
    
	  // div id, variable names	  
		$elements->tagname = $prop['tagid'];
		$elements->tagid = strtolower(preg_replace('/ /', '-', $prop['tagid']));
		$elements->tagvar = strtolower(preg_replace('/ /', '_', $prop['tagid']));
		$elements->type = $prop['type'];
		$elements->iw = $prop['iw'];
		$elements->output_type = $output_type;
		
		// wrapper divs						    
		$wrapper_div = "\n$tab<div id=\"" . $elements->tagid . '" class="wrapper wrapper-' . $elements->tagid . $elements->classes . '">';
		$wrapper_div_end = '</div><!-- /#' . $elements->tagid . ' -->';	
				
		// inner wrapper switch
		if ($elements->iw) { 
		  $wrapper_div .= '<div id="' . $elements->tagid . '-inner" class="wrapper-inner">';	
		  $wrapper_div_end  = '</div></div><!-- /#' . $elements->tagid . '-inner, /#' . $elements->tagid . ' -->'; 
		} 
					
		// process regions, reference function
    om_layout_regions($elements, $tab);
					
    if (isset($om_layout_content[$cid])) { //start recursion of div id is a parent
      
      // put wrapper div if parent
			$output .= $wrapper_div;
			
			//start recursion
      om_subthemer_layout($om_layout_content[$cid], $output_type); 
      
      // put end wrapper div
      $output .= "\n$tab" . $wrapper_div_end;
      
    } else {
      
      // for creating regions on .info file
      if (($elements->type != 'variable') && ($elements->tagname != 'Footer Message' ) && ($elements->output_type != 'html' )) $om_regions[] = $elements->tagname;
      
      // additional classes for OM regions, used in OM page.tpl.php
 		  if (($elements->type != 'variable') && ($elements->output_type == 'om')) $om_region_classes .= "\n" . '$' . $elements->tagvar . '_classes = \'region-' . $elements->tagid . $elements->classes . '\';';     
      
      // get region contents
      $output .= om_get_value($elements, $tab); 
		}

		//adds float clear below the columns
		if (($elements->type == 'column') && ($div_count == $div_count_total)) $output .= "\n$tab<div class=\"clearfix\"></div>";
  }
	$count++;

	//switch to start the output
  if($count == $om_count) {
    $count = 0; // reset
    $level = 0; // reset
    $out_content = $output;
    
    // use reset because this function is being used more than one,
    // this prevents from adding previous content to the output
    $output = ''; 
    
    return $out_content;
  }
}


/**
 * Get default html content
 *
 * @param $elements, includes:
 *
 *   Property          Sample Values
 *   - tagname         - Content Elements
 *   - tagid           - content-elements
 *   - tagvar          - content_elements
 *   - type            - variable  
 *   - output_type     - html 
 *   - region          - <div ... 
 *   - region_end      - .../div>   
 *   - var_wrapper     - <div ... 
 *   - var_wrapper_end - .../div>   
 *   - iw              - 0  
 *
 */ 
function om_get_value($elements, $tab = NULL) {
  global $om_content_variables;
  global $om_subthemer_blocks;
  
  $node = $om_content_variables['node'];
  
  //dsm($om_subthemer_blocks);
  if (!empty($om_subthemer_blocks[$elements->tagvar])) {
    // check for the assigned blocks for each region and add its content
    $region_content = $om_subthemer_blocks[$elements->tagvar];
  }
  else {
    // default html region content
    $region_content = "\n$tab\t<h3 class=\"region-title\">" . $elements->tagid . '</h3>';	
  }

  // the variable exists in the database   
	if (isset($om_content_variables[$elements->tagname])) {
	  $value = array();
  
    // TODO: somehow I think, I should be using php on preview
    if (($elements->output_type == 'html')) {
      // html value for preview
      $value = $om_content_variables[$elements->tagname]['value_html'];
    }
    else {
      // php value for page.tpl.php
      $value = $om_content_variables[$elements->tagname]['value_php'];
    }

    // $content variable source
    ob_start();
    eval($value);
    ob_end_clean();  

    return "\n$tab" . $content; 
  }
  else { // the variable doesn't exist in the database
  
    if ($elements->type == 'variable') {
      // get default variable value
      $tagname = 'Variable';
    }
    else {
      // get default region value
      $tagname = 'Region';
    }
    
    // TODO: somehow I think, I should be using php instead of html file on preview
    if (($elements->output_type == 'html')) {    
      // html value for preview
      $value = $om_content_variables[$tagname]['value_html'];
    }
    else {
      // php value for page.tpl.php
      $value = $om_content_variables[$tagname]['value_php'];
    }

    // $content variable source
    ob_start();
    eval($value);
    ob_end_clean(); 
     
    // it is a variable
    return "\n$tab" . $content; 
  }
}


/**
 * Generates divs region divs
 *
 * TODO: refactor this function
 *
 */ 
function om_layout_regions(&$elements, $tab = NULL) {

  if ($elements->output_type == 'html') { // html preview tags
		// regions divs  		
		if ($elements->type != 'variable') {
		  // region divs
      $elements->wrapper = "\n$tab<div id=\"" . $elements->tagid . '" class="region region-' . $elements->tagid . $elements->classes . '">';
		  $elements->wrapper_end = '</div><!-- /#' . $elements->tagid . ' -->';
		}
		elseif ($elements->type == 'variable') {
      $elements->var_wrapper = "\n$tab" . '<div id="' . $elements->tagid . '">';
		  $elements->var_wrapper_end = '</div><!-- /#' . $elements->tagid . ' -->';
		}  
		// for inner wrapper
		if ($elements->iw && ($elements->type != 'variable')) { 
			$elements->wrapper .= '<div id="' . $elements->tagid . '-inner" class="region-inner">';
			$elements->wrapper_end  = '</div></div><!-- /#' . $elements->tagid . '-inner, /#' . $elements->tagid . ' -->';
		}   
  
  }
  elseif ($elements->output_type == 'om') { // om doesn't need these wrapper settings
      $elements->wrapper = "\n$tab" . '';
		  $elements->wrapper_end = '';
	}
	else { // php default tags and conditionals
		// regions divs  		
		if ($elements->type != 'variable') {
		  // region divs
      $elements->wrapper = '<?php if (!empty($' . $elements->tagvar . ')) : ?>' . "<div id=\"" . $elements->tagid . '" class="region region-' . $elements->tagid . $elements->classes . '">';
		  $elements->wrapper_end = '</div><!-- /#' . $elements->tagid . ' --><?php endif; ?>';
		}
		elseif ($elements->type == 'variable') {
      $elements->wrapper = "\n$tab" . '<div id="' . $elements->tagid . '">';
		  $elements->wrapper_end = '</div><!-- /#' . $elements->tagid . ' -->';
		}  
		// for inner wrapper
		if ($elements->iw && ($elements->type != 'variable')) { 
			$elements->wrapper .= '<div id="' . $elements->tagid . '-inner" class="region-inner">';
			$elements->wrapper_end  = '</div></div><!-- /#' . $elements->tagid . '-inner, /#' . $elements->tagid . ' --><?php endif; ?>';
		}   
  }
}

		
/**
 * Generates divs classes
 *
 */ 
function om_layout_classes($div_count_total = NULL, $div_count = NULL, $prop = array()) {
  $classes = '';
	  
  // row/column class
  $type = $prop['type']; 
	$classes .= ' ' . $type;
		
	// row/column-total
	$classes .= ' ' . $type . '-' . $div_count_total;
    
  // row/column-total-count row/column-count
	$classes .= ' ' . $type . '-' . $div_count_total . '-' . $div_count;
	$classes .= ' ' . $type . '-' . $div_count;
		
	// first/last
	if ($div_count == 1) $classes .= ' first';
	if ($div_count == $div_count_total) $classes .= ' last';
		
	// wrapper-outer
	if ($prop['pid'] == 0) $classes .= ' wrapper-outer';
	
  return $classes;
}


/**
 * Get Content
 *
 * @param $content
 *   Variable name
 * @param $node
 *
 *
function om_get_variable_content($content = NULL, $node) {
  $file = drupal_get_path('module', 'om_subthemer') .'/contents/' . $content . '.txt';
  
  if (is_file($file)) {
    $file_content = file_get_contents($file);
    //dsm($file_content);
    // loads content
    if (!empty($file_content)) {
      ob_start();
      eval($file_content);
      ob_end_clean();
    }
    else {
      // prevents returning the variable name
      $content = '';
    }
  }
  else {
    // prevents returning the variable name
    $content = '';
  }

  return $content;
}
*/

/**
 * Get Content
 *
 * @param $content
 *   Variable name
 * @param $node
 *
 */
function om_get_variable($type = NULL, $node) {
  
  //vid, variable, value, source, type
  $result = db_query("SELECT vid, variable, value_html, value_php, source, type 
                      FROM {om_subthemer_vars} 
                      WHERE type = '%s' 
                      ORDER BY variable ASC", 
                      $type);

  $vars = array();
  while ($record = db_fetch_object($result)) {
		$vars[$record->variable]['variable'] = $record->variable;
		$vars[$record->variable]['value_html'] = $record->value_html;
		$vars[$record->variable]['value_php'] = $record->value_php;
		$vars[$record->variable]['source'] = $record->source;
		$vars[$record->variable]['type'] = $record->type;
  }
  $vars['node'] = $node;
  return $vars;
}





