<?php
/**
 * @file
 * OM Subthemer Import
 *
 */
 
/**
 * OM Subthemer import blocks form
 *
 */
function om_blocks_import(&$form_state) {

  $form['import'] = array(
    '#type' => 'fieldset',
    '#title' => t('Import Blocks'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#tree' => TRUE,
  );
  $form['import']['nid'] = array(
    '#type' => 'hidden',
    '#value' => arg(1),
  );    
  $form['import']['blocks'] = array(
    '#type' => 'textarea',
    '#title' => t(''),
    '#rows' => 30,    
    '#default_value' => '',
  );  
  $form['submit'] = array( '#type' => 'submit', '#value' => t('Import'), );  
  
	return $form;
}

/**
 * OM BLocks Import Submit
 * 
 */
function om_blocks_import_submit($form, $form_state) {
  
  $form_values = $form_state['values'];

  // Delete an existing blocks
  db_query("DELETE FROM {om_subthemer_blocks} WHERE nid = %d", $form_values['import']['nid']);
  
  ob_start();
  eval($form_values['import']['blocks']);
  ob_end_clean();
  
  if (is_array($blocks)) {
    om_subthemer_blocks_upload($blocks, $form_values['import']['nid']);
  }
  
  // new layout file has to be written for this new imported layout
  
  // need this to generate the layout
  //include_once drupal_get_path('module', 'om_subthemer') . '/inc/om_subthemer.write.inc';    

  // need to update the files
  //$node = node_load($form_values['import']['nid']);

  // set user folder 
  //om_subthemer_user_folder(&$node);

  // 1) get layout for variables
  //om_process_layout(&$node);
    
  // 2) write om subtheme
  //om_file_write($node);   
    
  //drupal_set_message(t('New layout has been generated'));
  
  // redirect of view node
  drupal_goto('node/' . $form_values['import']['nid'] . '/om-blocks');    
}


