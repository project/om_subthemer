<?php
/**
 * @file
 * Writes OM Subtheme files
 *
 */


/**
 * Writes:
 * - index.html
 * - page.tpl.php
 * - subtheme.info
 * - subtheme.zip
 *
 */
function om_file_write($node) {	
	
	// create directory and index html file	
	$index_html = om_index_html_file($node);
  om_file_create($index_html, $node->om_user_files , 'index.html');

	// create directory and index php file	
	$index_php = om_index_php_file($node);
  om_file_create($index_php, $node->om_user_files , 'page.tpl.php');
  
  // previous info file must be deleted if the node title has changed
  om_info_delete($node);
    
	// create info file	
	$info = om_info_file($node);
	$info_name = strtolower(preg_replace('/ /','_', $node->title));
  om_file_create($info, $node->om_user_files , $info_name . '.info');
  
	// increase script timeout value
	ini_set("max_execution_time", 300);
	// create object
	$zip = new ZipArchive();

	// open archive
	if ($zip->open($node->om_user_files . '.zip', ZIPARCHIVE::OVERWRITE) !== TRUE) {
		die ("Could not open archive");
	}

	// pass it the directory to be processed
	$iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($node->om_user_files . '/'));
	// add each file found to the archive
	foreach ($iterator as $key => $value) {
	  //dsm($key);
	  // this is the way to delete the absolute path, /sites/...
	  $path = explode('/', $key);
	  while ($path[0] != $node->om_user_folder) {
	    array_shift($path);
	  }
	  // delete last path which is the user code u10001d10001
	  array_shift($path);
	  
	  // transform array to path and add the node title as the folder
	  $path_file = $info_name . '/' . implode('/', $path);
	  
	  // get file names
	  $file = array_reverse(explode('/', $key));
	  
	  // include only subtheme files
	  if ($file[0]!= '.' && $file[0] != '..' && $file[0] != 'index.html' && $file[0] != 'default.layout') {     
		  $zip->addFile(realpath($key),  $path_file) or die ("ERROR: Could not add file: $key");
		}
		
	}
	// close and save archive
	$zip->close();	
}


/**
 * Delete info file prior to creation, the user changed the node title
 *  
 * Add doctype, head, body tags to processed divs as html
 *
 */


function om_index_html_file($node) {
  global $om_regions;
  
  $base_theme = variable_get('om_subthemer_'. $node->type . '_base_theme', 'om');
  
  if (($base_theme == 'om') || ($base_theme == 'zen')) {
    $left = 'first';
    $right = 'second';
  }
  else {
    $left = 'left';
    $right = 'right';
  
  }
  // layout and body classes.
  $layout = 'none';  
  $body_classes = 'front not-logged-in'; 
     
  if (in_array('Sidebar First', $om_regions)) {
    $layout = $left;
  }
  if (in_array('Sidebar Second', $om_regions)) {
    $layout = ($layout == $left) ? 'both' : $right;
  }

  // Add information about the number of sidebars.
  if ($layout == 'both') {
    $body_classes .= ' two-sidebars';
  }
  elseif ($layout == 'none') {
    $body_classes .= ' no-sidebars';
  }
  else {
    $body_classes .= ' one-sidebar sidebar-'. $layout;
  }
    
	$output = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">';
	$output .= "\n" . '<html xmlns="http://www.w3.org/1999/xhtml">';
	$output .= "\n" . '<head>';
	$output .= "\n\t" . '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
	$output .= "\n\t" . '<link type="image/x-icon" href="favicon.ico" rel="shortcut icon">';	
	$output .= "\n\t" . '<title>' . $node->title . ' - (' . $node->name . ')</title>';
  
  // get all stylesheets
	$css = drupal_add_css();
	
	// remove active base theme
	unset($css['all']['theme']);
	
	// print all stylesheets
	$output .= "\n\t" . drupal_get_css($css);
	
	if ($base_theme != 'none') {

    // get stylesheets from active base theme	
    $stylesheets = om_base_theme_file($node->type, 'stylesheets'); 
    foreach ($stylesheets as $style_key => $style) {
	    $output .= "\n\t" . '<link type="text/css" rel="stylesheet" media="all" href="' . base_path() . drupal_get_path('theme', $base_theme) . '/' . $style . '" />';	
	  }	
	}
		
	$output .= "\n\t" . '<link type="text/css" rel="stylesheet" media="all" href="css/style.css" />';
	
	// get all scripts
  $js = drupal_add_js();
  
  // remove js from active base theme
	unset($js['theme']);
	
	// prints the scripts
	$output .= "\n\t" . drupal_get_js('header', $js);

	if ($base_theme != 'none') {
	
	  // get scripts from active base theme
    $scripts = om_base_theme_file($node->type, 'scripts');
    foreach ($scripts as $script_key => $script) {
	    $output .= "\n\t" . '<script type="text/javascript" src="' . base_path() . drupal_get_path('theme', $base_theme) . '/' . $script . '" ></script>';	
	  }	
	}	
	
  $output .= "\n\t" . '<script type="text/javascript" src="js/script.js" ></script>';
  
  $output .= "\n" . '</head>';
	$output .= "\n" . '<body class="' . $body_classes . '">';
	$output .= "\n" . $node->om_layout_html_content;
  $output .= "\n" . '<div id="legal"><a href="http://www.drupal.org/project/om_subthemer">OM SubThemer</a> ' . date('d-m-Y h:i:s A') . ' | V6.x-1.x | by <a href="http://www.danielhonrade.com">Daniel Honrade</a></div>';
	$output .= "\n\n" . '</body>';
	$output .= "\n" . '</html>';
	return $output;
}


/**
 * Delete info file prior to creation, the user changed the node title
 *  
 * Adds doctype, head, body tags to processed divs as php
 *
 */
function om_index_php_file($node) {

  $base_theme = variable_get('om_subthemer_'. $node->type . '_base_theme', 'om');
  
  $subtheme = strtolower(preg_replace('/ /','_', $node->title));
  	
  $output = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">';
  $output .= "\n" . '<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">';
  $output .= "\n" . '<head>';
  $output .= "\n\t" . '<?php print $head; ?>';
  $output .= "\n\t" . '<title><?php print $head_title; ?></title>';
  $output .= "\n\t" . '<?php print $styles; ?>';
  $output .= "\n\t" . '<!--[if IE  ]><?php print ' . $subtheme . '_get_ie_styles(\'ie\'); ?><![endif]-->';
  $output .= "\n\t" . '<!--[if IE 6]><?php print ' . $subtheme . '_get_ie_styles(\'ie6\'); ?><![endif]-->';
  $output .= "\n\t" . '<!--[if IE 7]><?php print ' . $subtheme . '_get_ie_styles(\'ie7\'); ?><![endif]-->';
  $output .= "\n\t" . '<!--[if IE 8]><?php print ' . $subtheme . '_get_ie_styles(\'ie8\'); ?><![endif]-->';
  $output .= "\n\t" . '<!--[if IE 9]><?php print ' . $subtheme . '_get_ie_styles(\'ie9\'); ?><![endif]-->';
  $output .= "\n\t" . '<?php print $scripts; ?>';
  $output .= "\n\t" . '<script type="text/javascript"><?php /* Needed to avoid Flash of Unstyled Content in IE */ ?> </script>';
  $output .= "\n" . '</head>';
  $output .= "\n" . '<body class="<?php print $body_classes; ?>">';
  
  if (!empty($node->om_region_classes)) {
    $output .= "\n\n" . '<?php';
	  $output .= $node->om_region_classes;  
    $output .= "\n" . '?>';	
  }
	
	$output .= "\n" . $node->om_layout_php_content;
  $output .= "\n" . '<?php print $closure; ?>';
  $output .= "\n\n" . '</body>';
	$output .= "\n" . '</html>';
	return $output;
}


/**
 * Delete info file prior to creation, the user changed the node title
 * - file write
 *
 */
function om_info_delete($node) {

  $dir = $node->om_user_files;
  $info_file = scandir($dir);
    
  //array_shift($files); // removes '.'
  //array_shift($files); // removes '..'
    
  foreach ($info_file as $file) {
    $file_name_array = explode('.', $file);
    $file = $dir . '/' . $file;
      if ($file_name_array[1] == 'info') unlink($file);
  }
}


/**
 * Info file
 * 
 * - adds/updates regions and OM Subtheme name
 *
 */
function om_info_file($node) {
	global $om_regions;
	
	$base_theme = variable_get('om_subthemer_'. $node->type . '_base_theme', 'om');
	
  $output = '';
  $output .= '; name, description, screenshot, version - these are necessary for theme list';
	$output .= "\n";
  $output .= "\n" . 'name        = ' . $node->title . ' - Created by ' . ucwords($node->name);
  $output .= "\n" . 'description = "This a subtheme of OM Base Theme. For more info, read the <a href="http://drupal.org/node/1056254">OM Theme Documentation</a>"';
  $output .= "\n" . 'screenshot  = "screenshot.png"';
	$output .= "\n";
  
  if ($base_theme != 'none') $output .= "\n" . 'base theme = ' . $base_theme;
	
	$output .= "\n";
  $output .= "\n" . '; compatibilities';
  $output .= "\n" . 'core        = "6.x"';
  $output .= "\n" . 'engine      = phptemplate';
	$output .= "\n";
  $output .= "\n" . '; default stylesheets';
	$output .= "\n";
  $output .= "\n" . 'stylesheets[all][] = "css/style.css"';
	$output .= "\n";
  $output .= "\n" . '; jquery scripts';
  $output .= "\n" . 'scripts[]       = "js/script.js"';
	$output .= "\n";
  $output .= "\n" . '; default regions, on the left inside the brackets'; 
  $output .= "\n" . '; are the variable names used in page.tpl.php,';
  $output .= "\n" . '; on right side are the names necessary for blocks list';
	$output .= "\n";
	foreach($om_regions as $key => $region) {
		$output .= "\n" . 'regions[' . strtolower(preg_replace('/ /', '_', $region)) . '] = ' . $region;
	}
	$output .= "\n";
  $output .= "\n" . '; default features for theme settings';
  $output .= "\n" . 'features[] = logo';
  $output .= "\n" . 'features[] = name';
  $output .= "\n" . 'features[] = slogan';
  $output .= "\n" . 'features[] = mission';
  $output .= "\n" . 'features[] = node_user_picture';
  $output .= "\n" . 'features[] = comment_user_picture';
  $output .= "\n" . 'features[] = search';
  $output .= "\n" . 'features[] = favicon';
  $output .= "\n" . 'features[] = primary_links';
  $output .= "\n" . 'features[] = secondary_links';
  $output .= "\n" . 'features[] = breadcrumbs';	
	$output .= "\n";
	return $output;
}
