<?php
/**
 * @file
 * OM Subthemer Content Types UI
 * For users with admin access
 *  - admin can assign which content type to be used in OM Subtheming
 */

/**
 * Configure Form
 * call back for admin/settings/om-subthemer menu
 */ 
function om_subthemer_admin() {

  drupal_set_title(t('OM Subthemer Settings'));
  
  $form = array();
  
  $form['om_subthemer'] = array(
    '#type' => 'fieldset',
    '#title' => t('Subtheme Types'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#tree' => TRUE,
    '#description' => t('The OM Subthemer will be attached to the following content types if checked.'),
  );
  
  // scan themes and skins
  $base_themes   = om_folders_get(drupal_get_path('module', 'om_subthemer') . '/base_themes');
  $skins = om_folders_get(drupal_get_path('module', 'om_subthemer') . '/skins');  
  
  // All node types have 0 for not using surveys and 1 for using surveys
  foreach (node_get_types() as $type => $info) {
    $configure[] = _om_subthemer_admin($info, $type, $base_themes, $skins);
  }

  $form['om_subthemer'] += $configure;  

  $form['submit'] = array( '#type' => 'submit', '#value' => t('Save Settings'), 
  );

  return $form;
}

/**
 * Configure form
 *
 */
function _om_subthemer_admin($info, $type, $base_themes, $skins) {
  $form['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t($info->name),
    '#default_value' => variable_get('om_subthemer_'. $type . '_enabled', 0),
  ); 
  
  $form['type'] = array(
    '#type' => 'hidden',
    '#default_value' => $type,
  );

  $form['base_theme'] = array(
    '#type' => 'select',
    '#title' => t('Base Theme'),
    '#default_value' => variable_get('om_subthemer_'. $type . '_base_theme', 'om'),
    '#options' => $base_themes,
  );
  
  $form['default_skin'] = array(
    '#type' => 'select',
    '#title' => t('Default Skin'),
    '#default_value' => variable_get('om_subthemer_'. $type . '_default_skin', 'starter'),
    '#options' => $skins,
  );
      
  return $form; 
}

/**
 * Submit
 * 
 */
function om_subthemer_admin_submit($form, $form_state) {
  
  $form_values = $form_state['values'];
  
  //dsm($form_values['om_subthemer']);
  foreach ($form_values['om_subthemer'] as $admin) {
    //dsm($admin);
    variable_set('om_subthemer_'. $admin['type'] . '_enabled', $admin['enabled']);
    variable_set('om_subthemer_'. $admin['type'] . '_base_theme', $admin['base_theme']);
    variable_set('om_subthemer_'. $admin['type'] . '_default_skin', $admin['default_skin']);
  } 
  
  // for the new tab to appear
  cache_clear_all();
  menu_rebuild();  
}


/**
 * Implementation of theme_hook()
 * - View in table format
 *
 */
function theme_om_subthemer_admin($form) {
  $rows = array();
  $output = '';
  //dsm($form['settings']);
  foreach (element_children($form['om_subthemer']) as $key) {
    $row = array();
    // Strips labels
    //$form['om_subthemer'][$key]['enabled']['#title'] = '';
    $form['om_subthemer'][$key]['base_theme']['#title'] = '';
    $form['om_subthemer'][$key]['default_skin']['#title'] = '';

    $row[] = drupal_render($form['om_subthemer'][$key]['enabled']);
    $row[] = drupal_render($form['om_subthemer'][$key]['base_theme']);
    $row[] = drupal_render($form['om_subthemer'][$key]['default_skin']);
    $rows[] = $row;
  }

  $header = array('Enabled', 'Base Theme', 'Default Skin');

  // Header
  $form['om_subthemer']['#children'] = theme('table', $header, $rows);

  $output .= drupal_render($form);
  
  return $output;
}

