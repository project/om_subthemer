<?php
/**
 * @file
 * Subthemer Settings UI Form
 * - OM Subthemes
 *
 */

/**
 * Menu callback for admin/settings/surveys/[node_type]/settings.
 */
function om_settings_edit(&$form_state, $type, $name) {

  drupal_set_title(t('Settings for %type nodes', array('%type' => check_plain($name))));

  $form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t(''),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#tree' => TRUE,
  );
  //vid, variable, value, source, type
  $result = db_query("SELECT vid, variable, value_html, value_php, source, type 
                      FROM {om_subthemer_vars} 
                      WHERE type = '%s' 
                      ORDER BY variable ASC", 
                      $type);

  $vars = array();
  while ($record = db_fetch_object($result)) {
		$vars[] = _om_settings_edit($record);
  }
  //dsm($vars);
  $settings = array();
  if (empty($vars)) {
    $settings = om_base_theme_file($type, 'settings');
    
    om_subthemer_settings_upload($settings, $type);
    
    //vid, variable, value, source, type
    $result = db_query("SELECT vid, variable, value_html, value_php, source, type 
                      FROM {om_subthemer_vars} 
                      WHERE type = '%s' 
                      ORDER BY variable ASC", 
                      $type);

    $vars = array();
    while ($record = db_fetch_object($result)) {
		  $vars[] = _om_settings_edit($record);
    }    
  }
                   
  $record = new stdClass();
  $record->type = $type;
  _om_new_rows(&$vars, 5, $record, 'settings_edit');   
 
  $form['settings'] += $vars;  
  //dsm($form);
  $form['submit'] = array( '#type' => 'submit', '#value' => t('Save Settings'), );

  $form['export'] = array( '#type' => 'submit', '#value' => t('Export'), );

  $form['import'] = array( '#type' => 'submit', '#value' => t('Import'), );
    
  return $form;
}

/**
 * Layout Form
 *
 */
function _om_settings_edit($record) {
  
  $form['vid'] = array(
    '#type' => 'hidden',
    '#value' => $record->vid,
  );
  
  $form['type'] = array(
    '#type' => 'hidden',
    '#default_value' => $record->type,
  );  
  
  $form['variable'] = array(
    '#type' => 'textfield',
    '#title' => t('Variable'),    
    '#default_value' => $record->variable,
    '#size' => 30,
  );  
  
  $form['value_html'] = array(
    '#type' => 'textarea',
    '#title' => t('HTML Value'),
    '#default_value' => $record->value_html,
  );

  $form['value_php'] = array(
    '#type' => 'textarea',
    '#title' => t('PHP Value'),
    '#default_value' => $record->value_php,
  );  
  $form['source'] = array(
    '#type' => 'textfield',
    '#title' => t('Source'),
    '#default_value' => $record->source,
    '#size' => 30,
  );

  $form['delete'] = array(
    '#type' => ($form['vid']['#value']) ? 'checkbox' : 'hidden',
    '#title' => t('Delete'),
    '#default_value' => 0,
    '#description' => t(''),
  );    
  return $form;
}

/**
 * Implementation of theme_hook()
 * - View in table format
 *
 */
function theme_om_settings_edit($form) {
  $rows = array();
  $output = '';
  $count = 0;
  //dsm($form['settings']);
  foreach (element_children($form['settings']) as $key) {
    $count++;
    $row = array();
    // Strips labels
    $form['settings'][$key]['variable']['#title'] = '';
    $form['settings'][$key]['value_html']['#title'] = '';
    $form['settings'][$key]['value_php']['#title'] = '';
    $form['settings'][$key]['source']['#title'] = '';
    $form['settings'][$key]['delete']['#title'] = '';
    $row[] = array('class' => 'col-number', 'data' => $count . ' ' . drupal_render($form['settings'][$key]['vid']) . drupal_render($form['settings'][$key]['type']));
    $row[] = drupal_render($form['settings'][$key]['variable']);
    $row[] = drupal_render($form['settings'][$key]['value_html']);
    $row[] = drupal_render($form['settings'][$key]['value_php']);
    $row[] = drupal_render($form['settings'][$key]['source']);
    $row[] = drupal_render($form['settings'][$key]['delete']);
    $rows[] = $row;
  }

  $header = array('No.', 'Variable', 'HTML Value', 'PHP Value', 'Source', 'Delete');

  // Header
  $form['settings']['#children'] = theme('table', $header, $rows);

  $output .= drupal_render($form);
  
  return $output;
}


/**
 * Settings Submit
 * 
 */
function om_settings_edit_submit($form, $form_state) {
  $form_values = $form_state['values'];
  //dsm($form_values['settings']);
  if($form_state['values']['op'] == t('Save Settings')) {

    foreach ($form_values['settings'] as $vars) {
  
      $variable = ucwords(trim($vars['variable']));
      $source   = ucwords(trim($vars['source']));
    
      if ($vars['delete']) {
        // Delete an existing vars
        db_query("DELETE FROM {om_subthemer_vars} WHERE vid = %d", $vars['vid']);
        drupal_set_message(t('The variable (' .  $variable . ') has been deleted.'));
      }
      elseif ($vars['vid']) {
        // Update an existing vars  
        db_query("UPDATE {om_subthemer_vars} SET variable = '%s', value_html = '%s', value_php = '%s', source = '%s' WHERE vid = %d", 
         $variable, $vars['value_html'], $vars['value_php'], $source, $vars['vid']);
        drupal_set_message(t('The variable (' .  $variable . ') has been updated.'));
      }
      elseif (!$vars['vid'] && !empty($variable)) {
        // Create a new vars
        db_query("INSERT INTO {om_subthemer_vars} (variable, value_html, value_php, source, type) VALUES ('%s', '%s', '%s', '%s', '%s')", 
         $variable, $vars['value_html'], $vars['value_php'], $source, $vars['type']);
        drupal_set_message(t('The variable (' .  $variable . ') has been added.'));
      }
      else {
        // If no condition is set, nothing should happen
      }
    }
  }
  elseif($form_state['values']['op'] == t('Export')) {
    drupal_goto('admin/settings/om-subthemer/'. $form_values['settings'][0]['type'] . '/export');
  }
  elseif($form_state['values']['op'] == t('Import')) {
    drupal_goto('admin/settings/om-subthemer/'. $form_values['settings'][0]['type'] . '/import');
  }  
}



