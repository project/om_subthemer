<?php
/**
 * @file
 * OM Subthemer Import Settings
 *
 */
 
/**
 * OM Subthemer import settings form
 *
 */
function om_settings_import(&$form_state, $type, $name) {

  drupal_set_title(t('Import %type settings', array('%type' => check_plain($name))));
  
  $form['import'] = array(
    '#type' => 'fieldset',
    '#title' => t('Import Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#tree' => TRUE,
  );
  $form['import']['type'] = array(
    '#type' => 'hidden',
    '#value' => $type,
  );    
  $form['import']['settings'] = array(
    '#type' => 'textarea',
    '#title' => t(''),
    '#rows' => 30,    
    '#default_value' => '',
  );  
  $form['submit'] = array( '#type' => 'submit', '#value' => t('Import'), );  
  
	return $form;
}

/**
 * OM Layout Import Submit
 * 
 */
function om_settings_import_submit($form, $form_state) {
  
  $form_values = $form_state['values'];
  //dsm($form_values);
  // Delete an existing layout
  db_query("DELETE FROM {om_subthemer_vars} WHERE type = '%s'", $form_values['import']['type']);
  
  ob_start();
  eval($form_values['import']['settings']);
  ob_end_clean();
  //dsm($settings);
  if (is_array($settings)) {
    om_subthemer_settings_upload($settings, $form_values['import']['type']);
  }

  // new settings file has to be written for this new imported layout 
    
  drupal_set_message(t('New settings has been uploaded'));
  
  // redirect of view node
  drupal_goto('admin/settings/om-subthemer/' . $form_values['import']['type']);    
}


