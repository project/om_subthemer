<?php
/**
 * @file
 * Subthemer Settings UI Form
 * - OM Subthemes
 *
 */
 
/**
 * OM Skin form
 *
 */
function om_skin_select(&$form_state) {

  // I just need the node title, so better use this function to get the node title
  $title = drupal_get_title();
    
  drupal_set_title(t('Skin for %title', array('%title' => check_plain($title))));
    
  $form['skin'] = array(
    '#type' => 'fieldset',
    '#title' => t(''),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#tree' => FALSE,
  );
 
  // scan files for skins
  $skins = om_folders_get(drupal_get_path('module', 'om_subthemer') . '/skins');  
  
  // get skin setting for this node                      
  $result = db_query("SELECT nid, skin
                      FROM {om_subthemer}
                      WHERE nid = %d", 
                      arg(1));

  $record = db_fetch_object($result);
  $record->skins = $skins; // for options
  $layout[0] = _om_skin_select($record); 
 
  $form['skin'] += $layout;  

  $form['submit'] = array( '#type' => 'submit', '#value' => t('Save Settings'), );
  //dsm($form); 
  return $form;
}


/**
 * Skin Options
 *
 */
function _om_skin_select($record) {

  foreach ($record->skins as $key => $subtheme) {
    // included screenshots for the options
    $options[$key] = '<h5>' . $subtheme . '</h5><div class="screenshot"><img alt="' . $subtheme . '" src="' . base_path() . drupal_get_path('module', 'om_subthemer') .'/skins/' . $key . '/screenshot.png' . '" /></div>';
  }

  $form['nid'] = array(
    '#type' => 'hidden',
    '#value' => $record->nid,
  );  
  
  $form['skin'] = array(
    '#type' => 'radios',
    '#default_value' => $record->skin,
    '#options' => $options, 
  );

  return $form;
}

/**
 * Implementation of theme_hook()
 * 
 * Skin Table layout
 *
 */
function theme_om_skin_select($form) {

  drupal_add_css(drupal_get_path('module', 'om_subthemer') . '/css/om_subthemer.css');
  
  $rows = array();
  $output = '';
  //dsm($form['skin']);
  foreach (element_children($form['skin'][0]['skin']) as $key) {
    $row = array();
    // Strips labels
    $form['skin'][$key]['skin']['#title'] = '';
    $row[] = drupal_render($form['skin'][0]['skin'][$key]);
    $rows[] = array('class' => 'om-subtheme-list', 'data' => $row);
  }

  $header = array('Choose skin for your layout.');

  // Header
  $form['skin']['#children'] = theme('table', $header, $rows);

  $output .= drupal_render($form);
  
  return $output;
}


/**
 * Skin Submit
 * 
 */
function om_skin_select_submit($form, $form_state) {
  $form_values = $form_state['values'];
  //dsm($form_values);
  // Update an existing skin select  
  db_query("UPDATE {om_subthemer} SET skin = '%s' WHERE nid = %d", 
  $form_values['skin'], $form_values['nid']);
  
  // nicely format this skin name
  $skin = ucwords(preg_replace('/_/', ' ', $form_values['skin']));
  drupal_set_message(t($skin . ' has been selected.'));
  
  $node = node_load(arg(1));

	// set om_subthemes folder
	om_directory(OM_SUBTHEMER_FILE_DIRECTORY);
	
  // set user folder 
  om_subthemer_user_folder(&$node);

  // copy current subtheme files to user subtheme folder
	om_current_skin_copy($node);

}


