<?php
/**
 * @file
 * OM Subthemer Export Settings
 *
 */
 
/**
 * OM Subthemer export settings form
 *
 */
function om_settings_export(&$form_state, $type, $name) {

  drupal_set_title(t('Export %type settings', array('%type' => check_plain($name))));
  
  $result = db_query("SELECT vid, variable, value_html, value_php, source 
                      FROM {om_subthemer_vars} 
                      WHERE type = '%s'
                      ORDER BY variable ASC", 
                      $type);
  $vars = array();
  while ($record = db_fetch_object($result)) {
		$vars[] = array($record->variable, $record->value_html, $record->value_php, $record->source);
  }
  //dsm($vars);
  $out = '';
  // formats this layout to array variable type
  foreach ($vars as $key => $prop) {
    if (is_numeric($key)) {
      $out .= '
        $settings[' . $key . '] = array(
          \'variable\' => \'' . $prop[0] . '\', 
          \'value_html\' => \'' . addslashes($prop[1]) . '\', 
          \'value_php\' => \'' . addslashes($prop[2]) . '\', 
          \'source\' => \'' . $prop[3] . '\',
          );' . "\n";         
		} 						 		
  }
  
  $form['export'] = array(
    '#type' => 'fieldset',
    '#title' => t('Export Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#tree' => TRUE,
  );
  $form['export']['settings'] = array(
    '#type' => 'textarea',
    '#title' => t(''),
    '#rows' => 30,
    '#default_value' => $out,
  );  

  return $form;
}
