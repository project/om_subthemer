<?php
/**
 * @file
 * OM Subthemer Blocks Export
 *
 */
 
/**
 * OM Subthemer export blocks form
 *
 */
function om_blocks_export(&$form_state) {

  $result = db_query("SELECT title, content, module, region, weight 
                      FROM {om_subthemer_blocks} 
                      WHERE nid = %d 
                      ORDER BY weight ASC", 
                      arg(1));

  while ($record = db_fetch_object($result)) {
		$om_blocks_content[] = array($record->title, $record->content, $record->module, 'disabled', $record->weight);
  }
  
  $blocks = '';
  
  // formats this layout to array variable type
  foreach ($om_blocks_content as $key => $prop) {
    if (is_numeric($key)) {
      $blocks .= '
        $blocks[' . $key . '] = array(
          \'title\' => \'' . $prop[0] . '\', 
          \'content\' => \'' . addslashes($prop[1]) . '\', 
          \'module\' => ' . $prop[2] . ', 
          \'region\' => ' . $prop[3] . ',
          \'weight\' => ' . $prop[4] . ',
          );' . "\n";         
		} 						 		
  }
  
  $form['export'] = array(
    '#type' => 'fieldset',
    '#title' => t('Export Blocks'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#tree' => TRUE,
  );
  $form['export']['blocks'] = array(
    '#type' => 'textarea',
    '#title' => t(''),
    '#rows' => 30,
    '#default_value' => $blocks,
  );  

  return $form;
}
