<?php
/**
 * @file
 * OM Subthemer Export
 *
 */
 
/**
 * OM Subthemer export layout form
 *
 */
function om_layout_export(&$form_state) {

  $result = db_query("SELECT cid, pid, tagid, type, iw, weight, nid 
                      FROM {om_subthemer_layout} 
                      WHERE nid = %d 
                      ORDER BY weight ASC", 
                      arg(1));

  while ($record = db_fetch_object($result)) {
		$om_layout_content[] = array($record->tagid, $record->type, $record->iw, $record->weight, $record->cid, $record->pid);
  }
  
  $layout = '';
  
  // formats this layout to array variable type
  foreach ($om_layout_content as $key => $prop) {
    if (is_numeric($key)) {
      $layout .= '
        $layout[' . $key . '] = array(
          \'tagid\' => \'' . $prop[0] . '\', 
          \'type\' => \'' . $prop[1] . '\', 
          \'iw\' => ' . $prop[2] . ', 
          \'weight\' => ' . $prop[3] . ',
          \'cid\' => ' . $prop[4] . ',
          \'pid\' => ' . $prop[5] . '
          );' . "\n";         
		} 						 		
  }
  
  $form['export'] = array(
    '#type' => 'fieldset',
    '#title' => t('Export Layout'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#tree' => TRUE,
  );
  $form['export']['layout'] = array(
    '#type' => 'textarea',
    '#title' => t(''),
    '#rows' => 30,
    '#default_value' => $layout,
  );  

  return $form;
}
