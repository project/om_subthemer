$(document).ready(function() { 
  /*
  $("table#om-subthemer-layout td.col-tagid input.form-text").change(function() { 
    var val = $(this).parent().parent().parent().attr('id'); 
    //alert(val);
    $('#' + val + ' td.col-update input.form-checkbox').attr('checked', 'checked')
  }); 
  $("table#om-subthemer-layout td.col-type select").change(function() { 
    var val = $(this).parent().parent().parent().attr('id'); 
    //alert(val);
    $('#' + val + ' td.col-update input.form-checkbox').attr('checked', 'checked')
  }); 
  $("table#om-subthemer-layout td.col-iw input.form-checkbox").change(function() { 
    var val = $(this).parent().parent().parent().parent().attr('id'); 
    //alert(val);
    $('#' + val + ' td.col-update input.form-checkbox').attr('checked', 'checked')
  }); 
  $("table#om-subthemer-layout td.col-tagid a.tabledrag-handle").click(function() { 
    var val = $(this).parent().parent().attr('id'); 
    //alert(val);
    $('#' + val + ' td.col-update input.form-checkbox').attr('checked', 'checked')
  });  
  */  
  // currently variable shouldn't have inner divs set on layout
  $("table#om-subthemer-layout td.col-type select").change(function() { 
    var rowID = $(this).parent().parent().parent().attr('id'); 
    var val = $(this).val(); 
    //alert(rowID);
    if(val == 'variable') {
      $('#' + rowID + ' td.col-iw input.form-checkbox').attr('disabled', 'disabled')
    }
    else {
      $('#' + rowID + ' td.col-iw input.form-checkbox').attr('disabled', '')
    }
  }); 
}); 
