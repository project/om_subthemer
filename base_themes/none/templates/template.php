<?php



/**
 * Generates IE CSS links for LTR and RTL languages.
 */

function phptemplate_get_ie_styles($ie) {
  global $language;
  global $theme; 
  switch ($ie) {
    case 'ie' : $ie_path = '/css/ie.css'; break;
    case 'ie6': $ie_path = '/css/ie6.css'; break;
    case 'ie7': $ie_path = '/css/ie7.css'; break;
    case 'ie8': $ie_path = '/css/ie8.css'; break;
    case 'ie9': $ie_path = '/css/ie9.css'; break;
    default   : $ie_path = '/css/ie8.css'; break;
  }
  $iecss = '<link type="text/css" rel="stylesheet" media="all" href="'. base_path() . drupal_get_path('theme', $theme) . $ie_path . '" />';

  if ($language->direction == LANGUAGE_RTL) {
    $iecss .= '<style type="text/css" media="all">@import "'. base_path() . drupal_get_path('theme', $theme) .'/css/ie-rtl.css";</style>';
  }
  return $iecss;
}
